const inventory = require("./1-arrays-jobs.cjs")
function averageSalaryBasedOnCountry(inventory, country) {
    let filteredinventory = inventory.filter((profile) => profile.location == country);
    let totalEmployees = filteredinventory.length;
    let totalSalary = filteredinventory.reduce((acc, curr) => {
        return +(acc + Number(curr.salary.slice(1)));
    }, 0)
    return +(totalSalary / totalEmployees).toFixed(2);
}
console.log(averageSalaryBasedOnCountry(inventory, "Brazil"))

profiles