const inventory = require("./1-arrays-jobs.cjs")

const SumOfAllSalaries = inventory.map(data => {
    return +data.salary.replace('$', '')
}).reduce((previous, current) => {
    return previous + current
})
console.log(SumOfAllSalaries)
