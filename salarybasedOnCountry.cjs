const inventory = require("./1-arrays-jobs.cjs")
const SalaryBasedOnCountry = inventory.reduce((Previous, current) => {
    if (Previous[current.location]) {
        Previous[current.location] += (Number(current.salary.replace("$", "")))
    } else {
        Previous[current.location] = (Number(current.salary.replace("$", "")))

    }
    return Previous
}, {})
console.log(SalaryBasedOnCountry)